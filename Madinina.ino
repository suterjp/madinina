#include <avr/wdt.h>
#include <RunningAverage.h>

#include <iCompass.h>

#include <AP_Math_freeimu.h>

#include <Filter.h>
#include <LowPassFilter2p.h>
#include <Butter.h>
#include <AverageFilter.h>
#include <AltitudeComplementary.h>
#include <DerivativeFilter.h>
#include <FilterClass.h>
#include <FilterWithBuffer.h>
#include <ModeFilter.h>
#include <LowPassFilter.h>

#include <FilteringScheme.h>
#include <AP_Baro_MS5611.h>

#include <ADXL345.h>
#include <bma180.h>
#include <HMC58X3.h>
#include <MS561101BA.h>
#include <I2Cdev.h>
#include <MPU60X0.h>
#include <EEPROM.h>
#include <FlexiTimer2.h>
#include <AverageList.h>
#include <PString.h>

//#define DEBUG
#include "DebugUtils.h"
#include "CommunicationUtils.h"
#include "FreeIMU.h"
#include <Wire.h>
#include <SPI.h>
//#include <Adafruit_GPS.h>
#include <TinyGPS++.h>
#include <Adafruit_ADS1015.h>
#include <BMP085.h>
#include <SoftwareSerial.h>
#include <Adafruit_CC3000.h>
#include <ccspi.h>
#include "utility/debug.h"
#include "DateTime.h"
//#include <TimerThree.h>
void wdt_init(void) __attribute__((naked)) __attribute__((section(".init3")));

volatile boolean execute = false;
volatile boolean T200m = false;
volatile boolean T1s = false;
//volatile boolean T10s = false;
volatile boolean T1M = false;
volatile int interval = 0;
unsigned long currentTime =0;
unsigned long lastPolledTime = 0;   // Last value retrieved from time server
unsigned long sketchTime = 0;       // CPU milliseconds since last server query

float q[4];
float qm[7];
//float m[3];
int raw_values[11];
float ypr[3]; // yaw pitch roll
float yprm[4]; // yaw, pitch, roll, mag heading
float val[9];
float declination=0.8;
volatile bool Alimente=false;
bool wifi=false;
volatile bool led=true;
int ExtT=0;
float windM =0.0;
int windDir = 0;
bool windMlu = false;
char Str[200];

PString Sentence(Str, sizeof(Str));

typedef volatile float rval; //change float to the datatype you want to use
const byte MAX_NUMBER_OF_READINGS = 5;
rval mghStorage[MAX_NUMBER_OF_READINGS] = {0.0};
AverageList<rval> mghList = AverageList<rval>( mghStorage, MAX_NUMBER_OF_READINGS );

// rate of turn average
rval rotStorage[MAX_NUMBER_OF_READINGS] = {0.0};
AverageList<rval> rotList = AverageList<rval>( rotStorage, MAX_NUMBER_OF_READINGS);
// wind average
//rval windDStorage[MAX_NUMBER_OF_READINGS] = {0.0};
//AverageList<rval> windDList = AverageList<rval>( windDStorage, MAX_NUMBER_OF_READINGS);
rval windSStorage[MAX_NUMBER_OF_READINGS] = {0.0};
AverageList<rval> windSList = AverageList<rval>( windSStorage, MAX_NUMBER_OF_READINGS);
// WIFI
// These are the interrupt and control pins
#define ADAFRUIT_CC3000_IRQ   3  // MUST be an interrupt pin!
// These can be any two pins
#define ADAFRUIT_CC3000_VBAT  46
#define ADAFRUIT_CC3000_CS    53
// Use hardware SPI for the remaining pins
// On an UNO, SCK = 13, MISO = 12, and MOSI = 11
Adafruit_CC3000 cc3000 = Adafruit_CC3000(ADAFRUIT_CC3000_CS, ADAFRUIT_CC3000_IRQ, ADAFRUIT_CC3000_VBAT,
                                         SPI_CLOCK_DIVIDER); // you can change this clock speed but DI

//#define WLAN_SSID       "jpsB"        
//#define WLAN_PASS       "indimama12345"
#define WLAN_SSID       "MADININA"        
#define WLAN_PASS       "GREEN12345"
#define WLAN_SECURITY   WLAN_SEC_WPA2
//#define SERVER_IP       10, 4, 253, 12
#define SERVER_IP       46, 140, 104, 70
#define SERVER_PORT     8888
#define LED_PIN         42
uint32_t ip, ip_ntp;
            
//USB
//GPS
TinyGPSPlus GPS;
//Girouette (input) moteur/radio (output)
TinyGPSPlus Gir;
TinyGPSCustom windS(Gir, "WIMWV", 3);
TinyGPSCustom windD(Gir, "WIMWV", 1);
TinyGPSCustom ExtTemp(Gir, "YXXDR", 2);
//AIS (radio/input) MFD (output)
TinyGPSPlus AIS;
//ADS
Adafruit_ADS1115 ads;
// Set the FreeIMU object
FreeIMU my3IMU = FreeIMU();
//The command from the PC
char cmd;

void wdt_init(void)
{
MCUSR = 0;
wdt_disable();
return;
}

/*
 * Timer interrupt driven method to do time sensitive calculations
 * The calc flag causes the main loop to execute other less sensitive calls.
 */
void calculate() {
        //we create 100ms pings here
        execute = true;
        //we record the ping count out to 120 secs
        interval++;
        if (interval %   2 == 0) T200m = true;
        if (interval %  10 == 0) T1s = true;
//      if (interval % 100 == 0) T10s = true;
//      if (interval % 100 == 0) T1M = true;
        if (interval % 600 == 0) T1M = true;
        interval = interval % 18000;
}

void connectWIFI() {
        int timeout = 50; //5 secondes 
        wifi = cc3000.begin();
//  wifi = ((wifi) && (cc3000.deleteProfiles()));
        wdt_disable();
        wifi = (cc3000.connectToAP(WLAN_SSID, WLAN_PASS, WLAN_SECURITY),3);
        wdt_enable(WDTO_8S);
        while ((wifi) && (!cc3000.checkDHCP())) {
            delay(100); // ToDo: Insert a DHCP timeout
            timeout--;
            if (timeout == 0) wifi = false;
        }
        wdt_reset();
        ip = cc3000.IP2U32(SERVER_IP);
  // Hostname to IP lookup; use NTP pool (rotates through servers)
//      if(cc3000.getHostByName("pool.ntp.org", &ip_ntp)) {
//      } else ip_ntp = 0;

}

void setup() {
  int16_t adc1;
  Wire.begin();
  
//USB
  Serial.begin(115200);
//  Serial.begin(57600);
//  Serial.println(F("Debut setup"));

  wdt_enable(WDTO_8S);

  my3IMU.init(true);
  
  adc1 = ads.readADC_SingleEnded(1);
  Alimente = (adc1 > 18700);
  // LED
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, HIGH);
  mghList.reset();
  rotList.reset();
//  windDList.reset();
  windSList.reset();
//WIFI
  connectWIFI();
  digitalWrite(LED_PIN, LOW);
// ADS1115 //
  ads.setGain(GAIN_TWOTHIRDS);
  ads.begin();
// GPS     //
  Serial1.begin(9600);
  Serial1.println(F("$PMTK220,1000*1F"));
  Serial1.println(F("$PMTK314,0,1,1,1,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0*29"));
  Serial1.println(F("$PGCMD,33,0*6D"));
// Girouette (input) moteur/radio (output) //
  Serial2.begin(4800);
//AIS (radio/input) MFD (output)
  Serial3.begin(38400);
//
//  Serial.println(F("Fin setup"));
//  lastPolledTime = getTime();
  sketchTime = millis();

  //setup timers
  FlexiTimer2::set(100, calculate); // 100ms period
  FlexiTimer2::start();

  digitalWrite(LED_PIN, HIGH);
}

void loop() {
    if ((millis() - sketchTime) > 86400000) {
        delay(10000); // = reset !
//      lastPolledTime = getTime();
//      sketchTime = millis();
    }
    currentTime = lastPolledTime + (millis() - sketchTime) / 1000;
    while (Serial1.available() > 0) {
        if (GPS.encode(Serial1.read()))
          ReadGPS();
    }
    while (Serial2.available() > 0) {
        if (Gir.encode(Serial2.read()))
          ReadGir();
    }
    while (Serial3.available() > 0) {
        if (AIS.encode(Serial3.read()))
          ReadAIS();
    }
    if (windS.isUpdated()) {
        Sentence.begin();
        Sentence.print(windS.value()); 
        windSList.addValue(atof(Sentence));
        if (windMlu) {
            windMlu = false;
            windM = 0.0;
        }
        if (atof(Sentence) > windM) windM = atof(Sentence);
    }
    if (windD.isUpdated()) {
        Sentence.begin();
        Sentence.print(windD.value()); 
        windDir = atoi(Sentence);
//      windDList.addValue(atof(Sentence));
    }
    if (ExtTemp.isUpdated()) {
        Sentence.begin();
        Sentence.print(ExtTemp.value()); 
        ExtT = atoi(Sentence);
    }
    if (execute) {
        wdt_reset();
        my3IMU.getQ(q, val);
        if (T200m) {
            //mag 
            my3IMU.getValues(val);
            //convert to YPR
            my3IMU.getYawPitchRollRad(yprm);
            //convert to magnetic heading
            calcMagHeading();
            float rot=(val[5]*0.06097);
            rotList.addValue(-degrees(rot));
            mghList.addValue(yprm[3]);
        }
        if (T1s) {
            float h = degrees(mghList.getTotalAverage())+180;
//            if(h<0.0){
//              h=(360.0+h);
//            }
            printXDRVoltPres();
            printNmeaMag(h);
            printRateOfTurn();
            printPitchRoll();
            led = (!led);
            if (led)
              digitalWrite(LED_PIN, HIGH);
            else
              digitalWrite(LED_PIN, LOW);
        }
//      if (T10s) {
//              sendTOjps();
//      }
        if (T1M) {
//          if (!Alimente) 
            sendTOjps();
//        wifi=cc3000.checkConnected();
//        if (!wifi) connectWIFI();
        }
        execute = false;
        T1M = false;
        T1s = false;
        T200m = false;
//      T10s = false;
    }
}
   
void sendTOjps() {
    int timeout = 50; //5 secondes 
    wifi = true;
    if (!cc3000.checkConnected()) {
//      wdt_disable();
        wdt_reset();
        wifi = (cc3000.connectToAP(WLAN_SSID, WLAN_PASS, WLAN_SECURITY),1);
//      wdt_enable(WDTO_8S);
        wdt_reset();
        while ((wifi) && (!cc3000.checkDHCP())) {
            delay(100); // ToDo: Insert a DHCP timeout
            timeout--;
            if (timeout == 0) wifi = false;
        }
        wdt_reset();

    }
    if (!wifi) return;
    Adafruit_CC3000_Client connection = cc3000.connectTCP(ip, SERVER_PORT);
    wdt_reset();
    delay(50);
    if (connection.connected()) {
        int16_t adc0 ;
        float temp;
        double lat, lng, temp2;
        char chLat, chLng;
        int i;

  //XDR
        Sentence.begin();
        Sentence.print('%');
        Sentence.print(long(millis() / 1000));
        Sentence.print(',');
        //48V
        adc0 = ads.readADC_SingleEnded(0);
        Sentence.print(adc0*2.069/1000, 2);
        Sentence.print(',');
        temp = my3IMU.getBaroTemperature();
        Sentence.print(temp - 5.0, 1);
        Sentence.print(',');
        Sentence.print(ExtT);
        Sentence.print(',');
        temp = windSList.getTotalAverage();
        Sentence.print(temp, 1);
        Sentence.print(',');
//      temp = windDList.getTotalAverage();
        Sentence.print(windDir);
        Sentence.print(',');
        Sentence.print(windM, 1);
        Sentence.print(',');
        windMlu = true;
        temp = my3IMU.getBaroPressure();
        Sentence.print(int32_t(temp*100));
        Sentence.print(',');
        //pitch
        Sentence.print(degrees(yprm[1]), 1); //prints 1 decimal, but round to degree, should be good enough
        Sentence.print(',');
        //roll
        Sentence.print(degrees(yprm[2]), 1);
        Sentence.print(',');
        //yaw
        Sentence.print(degrees(yprm[0]) ,1);
        Sentence.print(',');
        Sentence.print(degrees(yprm[3])+180, 1);

  //RMC

        if ((GPS.time.isValid()) && (GPS.location.isValid())) {
                Sentence.print(',');
                i = GPS.date.year();
                Sentence.print(i);
                i = GPS.date.month();
                if (i < 10) Sentence.print('0');
                Sentence.print(i);
                i = GPS.date.day();
                if (i < 10) Sentence.print('0');
                Sentence.print(i);
                Sentence.print(',');
                i = GPS.time.hour();
                if (i < 10) Sentence.print('0');
                Sentence.print(i);
                i = GPS.time.minute();
                if (i < 10) Sentence.print('0');
                Sentence.print(i);
                i = GPS.time.second();
                if (i < 10) Sentence.print('0');
                Sentence.print(i);
                Sentence.print(',');
                lat = GPS.location.lat();
                if (lat < 0) {
                        lat = -lat;
                        chLat = 'S';
                } else chLat = 'N';
                lng = GPS.location.lng();
                if (lng < 0) {
                        lng = -lng;
                        chLng = 'W';
                } else chLng = 'E';
                if (lat <  10.0) Sentence.print('0');
                i = (int) lat;
                Sentence.print(i);
                Sentence.print('o');
                temp2 = ((lat - i) * 60.0);
                Sentence.print(temp2, 2);
                Sentence.print(',');
                Sentence.print(chLat);
                Sentence.print(',');
                if (lng <  10.0) Sentence.print('0');
                if (lng < 100.0) Sentence.print('0');
                i = (int) lng;
                Sentence.print(i);
                Sentence.print('o');
                temp2 = ((lng - i) * 60.0);
                Sentence.print(temp2, 2);
                Sentence.print(',');
                Sentence.print(chLng);
                Sentence.print(',');
                Sentence.print(GPS.speed.knots(), 1);
                Sentence.print(',');
                Sentence.print(GPS.course.deg(), 1);
                Sentence.print(',');
                if(declination<0.0) {
                        temp = -declination;
                } else {
                        temp = declination;
                }
                Sentence.print(temp, 1);
                Sentence.print(',');
                if(declination<0.0){
                  Sentence.print('E');
                }else{
                  Sentence.print('W');
                }
        } else {
            Sentence.print(',');
            DateTime.sync(currentTime);
            if (DateTime.available());
            Sentence.print(DateTime.Year + 1900);
            if (DateTime.Month < 9) Sentence.print('0');
            Sentence.print(DateTime.Month + 1);
            if (DateTime.Day < 10) Sentence.print('0');
            Sentence.print(DateTime.Day);
            if (DateTime.Hour < 10) Sentence.print('0');
            Sentence.print(DateTime.Hour);
            if (DateTime.Minute < 10) Sentence.print('0');
            Sentence.print(DateTime.Minute);
            if (DateTime.Second < 10) Sentence.print('0');
            Sentence.print(DateTime.Second);
        }
        Sentence.print('*');
        int cs = 0; //clear any old checksum
        for (unsigned int n = 1; n < Sentence.length() - 1; n++) {
            cs ^= Str[n]; //calculates the checksum
        }
        if (cs < 0x10) Sentence.print('0');
        Sentence.print(cs, HEX); // Assemble the final message and send it out the serial port
        Str[Sentence.length()-2] = toupper(Str[Sentence.length()-2]);
        Str[Sentence.length()-1] = toupper(Str[Sentence.length()-1]);

//      Serial.println(Sentence);
        Sentence.print('\r');
        Sentence.print('\n');
        connection.fastrprint(Str);
        delay(10);
        connection.close();
    }
}

/*

        1   2 3
        |   | |
 $--ROT,x.x,A*hh<CR><LF>

Field Number:

    Rate Of Turn, degrees per minute, "-" means bow turns to port

    Status, A means data is valid

    Checksum
   */
   
void printRateOfTurn() {

        Sentence.begin();
        Sentence.print(F("$TIROT,"));
        float r =rotList.getTotalAverage();
        Sentence.print(r, 1); //prints 1 decimal, but round to degree, should be good enough
        Sentence.print(F(",A*"));

        //calculate the checksum

        int cs = 0; //clear any old checksum
        for (unsigned int n = 1; n < (Sentence.length()) - 1; n++) {
                cs ^= Str[n]; //calculates the checksum
        }
        //bug - arduino prints 0x007 as 7, 0x02B as 2B, so we add it now
        if (cs < 0x10) Sentence.print('0');
        Sentence.print(cs, HEX); // Assemble the final message and send it out the serial port
        Str[Sentence.length()-2] = toupper(Str[Sentence.length()-2]);
        Str[Sentence.length()-1] = toupper(Str[Sentence.length()-1]);
        Serial.println(Sentence);
}
 
/*
 Print out the NMEA string for mag heading
 $HC = mag compass
            === HDM - Heading - Magnetic ===

            Vessel heading in degrees with respect to magnetic north produced by
            any device or system producing magnetic heading.
            
            ------------------------------------------------------------------------------
                    1   2 3
                    |   | |
             $--HDM,x.x,M*hh<CR><LF>
            ------------------------------------------------------------------------------
            
            Field Number: 
            
            1. Heading Degrees, magnetic
            2. M = magnetic
            3. Checksum
            
*/
void printNmeaMag(float h){
    //Assemble a sentence of the various parts so that we can calculate the proper checksum

        Sentence.begin();
        Sentence.print(F("$HCHDM,"));

        Sentence.print((int)h); //prints 1 decimal, but round to degree, should be good enough
        Sentence.print(F(".0,M*"));

        //calculate the checksum

        int cs = 0; //clear any old checksum
        for (unsigned int n = 1; n < Sentence.length() - 1; n++) {
                cs ^= Str[n]; //calculates the checksum
        }
        //bug - arduino prints 0x007 as 7, 0x02B as 2B, so we add it now
        if (cs < 0x10) Sentence.print('0');
        Sentence.print(cs, HEX); // Assemble the final message and send it out the serial port
        Str[Sentence.length()-2] = toupper(Str[Sentence.length()-2]);
        Str[Sentence.length()-1] = toupper(Str[Sentence.length()-1]);
//      Serial3.println(Sentence);
        Serial.println(Sentence);

        //do HDT if we have a declination
        if(declination!=0.0){
            //print HDT

                Sentence.begin();
                Sentence.print(F("$HCHDT,"));
                float d = h-declination;
                Sentence.print(d, 1); 
                Sentence.print(F(",T*"));
        
                //calculate the checksum
        
                int cs = 0; //clear any old checksum
                for (unsigned int n = 1; n < Sentence.length() - 1; n++) {
                        cs ^= Str[n]; //calculates the checksum
                }
                //bug - arduino prints 0x007 as 7, 0x02B as 2B, so we add it now
                if (cs < 0x10) Sentence.print('0');
                Sentence.print(cs, HEX); // Assemble the final message and send it out the serial port
                Str[Sentence.length()-2] = toupper(Str[Sentence.length()-2]);
                Str[Sentence.length()-1] = toupper(Str[Sentence.length()-1]);
//              Serial3.println(Sentence);
                Serial.println(Sentence);
                
                //and do HDG (Heading, Deviation and Variation)
                /*
                ------------------------------------------------------------------------------
                        1   2   3 4   5 6
                        |   |   | |   | |
                 $--HDG,x.x,x.x,a,x.x,a*hh<CR><LF>
                ------------------------------------------------------------------------------
                
                Field Number: 
                
                1. Magnetic Sensor heading in degrees
                2. Magnetic Deviation, degrees
                3. Magnetic Deviation direction, E = Easterly, W = Westerly, direction of magnetic north from true north.
                4. Magnetic Variation degrees
                5. Magnetic Variation direction, E = Easterly, W = Westerly
                6. Checksum
                
                */
                Sentence.begin();
                float x = (abs(declination));
                //x=((int)(x*10))*0.1;
                Sentence.print(F("$HCHDG,"));
                Sentence.print(h ,1);//mag heading 
                Sentence.print(',');
                Sentence.print(x, 1);//declination
                if(declination<0.0){
                  Sentence.print(F(",E,"));
                }else{
                  Sentence.print(F(",W,"));
                }
                Sentence.print(x, 1);//declination
                if(declination<0.0){
                  Sentence.print(F(",E*"));
                }else{
                  Sentence.print(F(",W*"));
                }
        
                //calculate the checksum
        
                cs = 0; //clear any old checksum
                for (unsigned int n = 1; n < Sentence.length() - 1; n++) {
                        cs ^= Str[n]; //calculates the checksum
                }
                //bug - arduino prints 0x007 as 7, 0x02B as 2B, so we add it now
                if (cs < 0x10) Sentence.print('0');
                Sentence.print(String(cs, HEX)); // Assemble the final message and send it out the serial port
                Str[Sentence.length()-2] = toupper(Str[Sentence.length()-2]);
                Str[Sentence.length()-1] = toupper(Str[Sentence.length()-1]);
//              Serial3.println(Sentence);
                Serial.println(Sentence);
        }
}
/*
$YXXDR (Transducer Measurements: Vessel Attitude) Used for various secondary transducers?!
or $PTNTHPR (Heading, Pitch, Roll) Looks like a new specific attitude one from the digital camera world as apparently EXIF tags also cont
ain NMEA strings.

$YXXDR,<1>, <2>, <3>, <4>,<5>, <6>, <7>, <8>,<9>, <10>,<11>,<12>,<13>,<14>,<15>,<16>*hh<CR><LF> 
eg $YXXDR,A,5.2,D,PTCH,A,7.4,D,ROLL,,,,,,,,*hh<CR><LF>

The fields in the B version of the XDR sentence are as follows:
<1>A = angular displacement
<2>Pitch: oscillation of vessel about its latitudinal axis. Bow moving up ispositive. Value reported to the nearest 0.1 degree.
<3>D = degrees
<4>PTCH (ID indicating pitch of vessel)
<5>A = angular displacement
<6>Roll: oscillation of vessel about its longitudinal axis. Roll to the starboard is positive. Value reported to the nearest 0.1 degree.
<7>D = degrees
<8>ROLL (ID indicating roll of vessel)
<9>+Not used
*/
void printPitchRoll(){
  
        Sentence.begin();

        Sentence.print(F("$YXXDR,A,"));
        //pitch
        Sentence.print(degrees(yprm[1]), 1); //prints 1 decimal, but round to degree, should be good enough
        Sentence.print(F(",D,PTCH,A,"));
        //roll
        Sentence.print(degrees(yprm[2]), 1);
        Sentence.print(F(",D,ROLL,A,"));
        //yaw
        Sentence.print(degrees(yprm[0]), 1);
        Sentence.print(F(",D,YAW*"));

        //calculate the checksum
        int cs = 0; //clear any old checksum
        for (unsigned int n = 1; n < Sentence.length() - 1; n++) {
                cs ^= Str[n]; //calculates the checksum
        }
        //bug - arduino prints 0x007 as 7, 0x02B as 2B, so we add it now
        if (cs < 0x10) Sentence.print('0');
        Sentence.print(String(cs, HEX)); // Assemble the final message and send it out the serial port
        Str[Sentence.length()-2] = toupper(Str[Sentence.length()-2]);
        Str[Sentence.length()-1] = toupper(Str[Sentence.length()-1]);
        Serial.println(Sentence);
        
}
   
void printXDRVoltPres(){
  
        int16_t adc0, adc1;
        float temp, press;

        Sentence.begin();

        adc0 = ads.readADC_SingleEnded(0);
        adc1 = ads.readADC_SingleEnded(1);
        Alimente = (adc1 > 18700);
        temp = my3IMU.getBaroTemperature();
        press = my3IMU.getBaroPressure();
        Sentence.print(F("$YXXDR,U,"));
        //48V
        Sentence.print(adc0*2.069/1000);
        Sentence.print(F(",V,BAT1,U,"));
        //Servitude
        Sentence.print(adc1*0.597/1000);
        Sentence.print(F(",V,BAT2,C,"));
        Sentence.print(temp, 1);
        Sentence.print(F(",C,ENV_INAIR_T,P,"));
        Sentence.print(int32_t(press*100));
        Sentence.print(F(",P,ENV_ATMOS_P*"));

        //calculate the checksum
        int cs = 0; //clear any old checksum
        for (unsigned int n = 1; n < Sentence.length() - 1; n++) {
                cs ^= Str[n]; //calculates the checksum
        }
        //bug - arduino prints 0x007 as 7, 0x02B as 2B, so we add it now
        if (cs < 0x10) Sentence.print('0');
        Sentence.print(String(cs, HEX)); // Assemble the final message and send it out the serial port
        Str[Sentence.length()-2] = toupper(Str[Sentence.length()-2]);
        Str[Sentence.length()-1] = toupper(Str[Sentence.length()-1]);
        Serial.println(Sentence);
}

//void ReadUSB() {
//    char *Sentence;
//
//    Sentence = USB.lastNMEA();
//    AIS_MFD.sendCommand(Sentence);
//    Gir_MR.sendCommand(Sentence);
//// echo pour DEBUG ....
//    USB.sendCommand(Sentence);
//}

void ReadGir() {
        Sentence.begin();

        Sentence.print(Gir.lastSentence());
        if (strstr(Str, "MWV")) {
            Serial3.println(Sentence);
        }
        Serial.println(Sentence);
}

void ReadAIS() {
        Sentence.begin();

        Sentence.print(AIS.lastSentence());
        Serial3.println(Sentence);
        Serial.println(Sentence);
}

void ReadGPS() {
        Sentence.begin();

        Sentence.print(GPS.lastSentence());
        if (strstr(Str, "PGTOP")) return;
        if (strstr(Str, "RMC")) {
            Serial2.println(Sentence);
        }// else {
        Serial.println(Sentence);
//      }
        Serial3.println(Sentence);
}

void calcMagHeading(){
  float Head_X;
  float Head_Y;
  float cos_roll;
  float sin_roll;
  float cos_pitch;
  float sin_pitch;
  
  cos_roll = cos(-yprm[2]);
  sin_roll = sin(-yprm[2]);
  cos_pitch = cos(yprm[1]);
  sin_pitch = sin(yprm[1]);
  
  //Example calc
  //Xh = bx * cos(theta) + by * sin(phi) * sin(theta) + bz * cos(phi) * sin(theta)
  //Yh = by * cos(phi) - bz * sin(phi)
  //return wrap((atan2(-Yh, Xh) + variation))
    
  // Tilt compensated Magnetic field X component:
  Head_X = val[6]*cos_pitch+val[7]*sin_roll*sin_pitch+val[8]*cos_roll*sin_pitch;
  // Tilt compensated Magnetic field Y component:
  Head_Y = val[7]*cos_roll-val[8]*sin_roll;
  // Magnetic Heading
  yprm[3] = atan2(-Head_Y,-Head_X);
  
}

// getTime function adapted from CC3000 ntpTest sketch.
// Minimalist time server query; adapted from Adafruit Gutenbird sketch,
// which in turn has roots in Arduino UdpNTPClient tutorial.
//unsigned long getTime(void) {
//  Adafruit_CC3000_Client client;
//  uint8_t       buf[48];
//  unsigned long startTime, t = 0L;
//  if (ip_ntp > 0) {
//    static const char PROGMEM
//      timeReqA[] = { 227,  0,  6, 236 },
//      timeReqB[] = {  49, 78, 49,  52 };
//    wdt_reset();
//    startTime = millis();
//    client = cc3000.connectUDP(ip_ntp, 123);
//    delay(50);
//    wdt_reset();
//    if(client.connected()) {
//      // Assemble and issue request packet
//      memset(buf, 0, sizeof(buf));
//      memcpy_P( buf    , timeReqA, sizeof(timeReqA));
//      memcpy_P(&buf[12], timeReqB, sizeof(timeReqB));
//      client.write(buf, sizeof(buf));
//
//      memset(buf, 0, sizeof(buf));
//      wdt_reset();
//      startTime = millis();
//      while((!client.available()) &&
//            ((millis() - startTime) < 4000));
//      wdt_reset();
//      if(client.available()) {
//        client.read(buf, sizeof(buf));
//        t = (((unsigned long)buf[40] << 24) |
//             ((unsigned long)buf[41] << 16) |
//             ((unsigned long)buf[42] <<  8) |
//              (unsigned long)buf[43]) - 2208988800UL;
//      }
//      wdt_reset();
//      client.close();
//    }
//    wdt_reset();
//  }
//  return t;
//}

