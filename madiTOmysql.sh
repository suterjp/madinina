#! /bin/bash  
IFS=','
while read -r date count battery Tint Text Vmoy Vdir Vmax pression roll pitch yaw mag dateUTC timeUTC lat NS long EW vitesse cap garbage
do  
  Press=`echo "scale=2; $pression/100" | bc`
  minutes=${lat:3:7}
  degr=${lat:0:2}
  latitude=`echo "scale=6; $degr+$minutes/60" | bc`
  minutes=${long:4:8}
  degr=${long:0:3}
  longitude=`echo "scale=6; $degr+$minutes/60" | bc`
  compteur="${count#%}"
#  epoch=`date --date=$date  "+%s"`
  epoch=${date:0:19}
  seconds=`date -d "$epoch" '+%s'`
  ag10m=`echo "scale=0; $seconds/600" | bc`
  agHou=`echo "scale=0; $seconds/3600" | bc`
  if [[ $Vmax == "99.9" ]]
  then
    Vmax="0.0" 
  fi
  echo -e "INSERT INTO Madinina.journal VALUES ('$epoch','$compteur','$battery','$Tint','$Text','$Vmoy','$Vmoy','$Vmax','$Press','$yaw','
$pitch','$roll','$mag','$dateUTC','$timeUTC','$latitude','$NS','$longitude','$EW','$vitesse','$cap','$ag10m','$agHou');"
done
