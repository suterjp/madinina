<?php
header('Content-type: text/plain');
$username = "xxx";
$password = "xxx";
$hostname = "xxx";
$database = "xxx";


// Opens a connection to a mySQL server
$connection=mysql_connect ($hostname, $username, $password);
if (!$connection) {
  die('Not connected : ' . mysql_error());
}

// Set the active mySQL database
$db_selected = mysql_select_db($database, $connection);
if (!$db_selected) {
  die ('Can\'t use db : ' . mysql_error());
}

// json output - insert table name below after "FROM"
$query = 'SELECT * FROM journal ORDER BY DateTimeRec DESC LIMIT 1;';
$dbquery = mysql_query($query);

if(! $dbquery )
{
  die('Could not get data: ' . mysql_error());
}

// Parse the dbquery into geojson 
// ================================================
// ================================================
// Return markers as GeoJSON

$geojson = array(
    'type'      => 'FeatureCollection',
    'features'  => array()
 );

while($row = mysql_fetch_assoc($dbquery)) {
    $feature = array(
        'type' => 'Feature', 
      'geometry' => array(
        'type' => 'Point',
        'coordinates' => array((float)$row['Longitude'], (float)$row['Latitude'])
            ),
      'properties' => array(
            'heure' => $row['dateTimeRec'],
            'Vitesse' => $row['Vitesse'],
            'Cap' => $row['MagnHead'],
            'Tint' => $row['TempInt'],
            'Text' => $row['TempExt'],
            'VentMoy' => $row['VentMoy'],
            'Pression' => $row['Pression'],
            'Vbat' => $row['VoltBat48V']
        //Other fields here, end without a comma
            )
        );
    array_push($geojson['features'], $feature);
};
mysql_close($connection);

// // Return routing result
    header("Content-Type:application/json",true);
    echo json_encode($geojson, JSON_NUMERIC_CHECK);
?>
