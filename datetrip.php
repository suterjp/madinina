<?php
header('Content-type: text/plain');
$username = "XXX";
$password = "XXX";
$hostname = "XXX";
$database = "XXX";


// Opens a connection to a mySQL server
$connection=mysql_connect ($hostname, $username, $password);
if (!$connection) {
  die('Not connected : ' . mysql_error());
}

// Set the active mySQL database
$db_selected = mysql_select_db($database, $connection);
if (!$db_selected) {
  die ('Can\'t use db : ' . mysql_error());
}

// json output - insert table name below after "FROM"
$query = "SELECT max(datetimerec) as dtr, avg(latitude) as lat, avg(longitude) as lon, avg(VentMoy) as VM, max(VentMax) as Vmax, AVG(Pression) as P, AVG(TempExt) as TE, AVG(TempInt) as TI, AVG(Vitesse) as V, AVG(Cap) as cap FROM journal where dateTimeRec > '".$_GET['debut']."' and dateTimeRec < '".$_GET['fin']."' group by latitude, longitude;";

//echo $query;

$dbquery = mysql_query($query);

if(! $dbquery )
{
  die('Could not get data: ' . mysql_error());
}

// Parse the dbquery into geojson 
// ================================================
// ================================================
// Return markers as GeoJSON

$geojson = array(
    'type'      => 'FeatureCollection',
    'features'  => array()  
 );

while($row = mysql_fetch_assoc($dbquery)) {
    $feature = array(
        'type' => 'Feature', 
      'geometry' => array(
        'type' => 'Point',
        'coordinates' => array((float)$row['lon'], (float)$row['lat'])
            ),
      'properties' => array(
            'heure' => $row['dtr'],
            'Vitesse' => round($row['V'], 1),
            'Cap' => round($row['cap'], 0),
            'Tint' => round($row['TI'], 1),
            'Text' => round($row['TE'], 1),
            'VentMoy' => round($row['VM'], 1),
            'VentMax' => round($row['Vmax'], 1),
            'Pression' => round($row['P'], 1)
        //Other fields here, end without a comma
            )
        );
    array_push($geojson['features'], $feature);
};
mysql_close($connection);

// // Return routing result
    header("Content-Type:application/json",true);
    echo json_encode($geojson, JSON_NUMERIC_CHECK);
?>
